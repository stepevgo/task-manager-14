package ru.t1.stepanishchev.tm.api.repository;

import ru.t1.stepanishchev.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository {

    Task create(String name, String description);

    Task create(String name);

    Task add(Task task);

    void clear();

    List<Task> findAll();

    List<Task> findAll(Comparator comparator);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    void remove(Task project);

    Task removeById(String id);

    Task removeByIndex(Integer index);

    Integer getSize();

    List<Task> findAllByProjectId(String projectId);

}